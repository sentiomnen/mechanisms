module pulley( id, tooth, pitch, height, angle ) {
	// Get Outer Radius
	radius = (pitch * tooth)/PI;

	difference() {
		union() {
			// Tooth
			difference() {
				union() {
					// Tooth
					cylinder(h = height, r = radius, center = true);

					for(i = [1:tooth]) {
						d = i * (360/tooth) + angle;
						l = radius;
						x = l * cos(d);
						y = l * sin(d);
						translate([x, y, 0]) cylinder(h = height, r = pitch/2, center = true);
					}
				}

				for(i = [1:tooth]) {
					d = i * (360/tooth) + (180/tooth) + angle;
					l = radius;
					x = l * cos(d);
					y = l * sin(d);
					translate([x, y, 0]) cylinder(h = height, r = pitch/2, center = true);
				}
			}

			// Roof
			difference() {
				cylinder(h = height + pitch, r = radius + pitch*2, center = true);
				cylinder(h = height, r = radius + pitch*2 + 1, center = true);
			}
		}
		
		// Shaft
		cylinder(h = height + pitch + 1, r= id, center = true);
	}

	render();
}



module belt_xz(locations, radiuses, thick, height) {
	length = len(locations);

	linear_extrude(height = height, center=true) {
	difference() {
		union() {
			for( i = [0:length-1] ) translate(locations[i]) circle(r = radiuses[i] + thick);
			for( i = [0:length-1] ) {
				if(i < length-1) {
					distance = sqrt(
						(locations[i][0] - locations[i+1][0])^2 +
						(locations[i][1] - locations[i+1][1])^2
					);

					unit    = locations[i+1] - locations[i];
					normal  = [unit[1], unit[0]];

					p1 = locations[i]   + (normal * (radiuses[i] + thick)   / distance);
					p2 = locations[i]   - (normal * (radiuses[i] + thick)   / distance);
					p3 = locations[i+1] + (normal * (radiuses[i+1] + thick) / distance);
					p4 = locations[i+1] - (normal * (radiuses[i+1] + thick) / distance);

					polygon(points=[p1, p2, p4, p3]);
			}}
		}

		union() {
			for( i = [0:length-1] ) translate(locations[i]) circle(r = radiuses[i] );
			for( i = [0:length-1] ) {
				if(i < length-1) {
					distance = sqrt(
						(locations[i][0] - locations[i+1][0])^2 +
						(locations[i][1] - locations[i+1][1])^2
					);

					unit    = locations[i+1] - locations[i];
					normal  = [unit[1], unit[0]];

					p1 = locations[i]   + (normal * (radiuses[i] )   / distance);
					p2 = locations[i]   - (normal * (radiuses[i] )   / distance);
					p3 = locations[i+1] + (normal * (radiuses[i+1] ) / distance);
					p4 = locations[i+1] - (normal * (radiuses[i+1] ) / distance);

					polygon(points=[p1, p2, p4, p3]);
			}}
		}
	}}

	render();
}

module belt_xz_tw(locations, radiuses, thick, height) {
	length = len(locations);

	linear_extrude(height = height, center=true) {
	difference() {
		union() {
			for( i = [0:length-1] ) translate(locations[i]) circle(r = radiuses[i] + thick);
			for( i = [0:length-1] ) {
				if(i < length-1) {
					distance = sqrt(
						(locations[i][0] - locations[i+1][0])^2 +
						(locations[i][1] - locations[i+1][1])^2
					);

					unit    = locations[i+1] - locations[i];
					normal  = [unit[1], unit[0]];

					p1 = locations[i]   + (normal * (radiuses[i] + thick)   / distance);
					p2 = locations[i]   - (normal * (radiuses[i] + thick)   / distance);
					p3 = locations[i+1] + (normal * (radiuses[i+1] + thick) / distance);
					p4 = locations[i+1] - (normal * (radiuses[i+1] + thick) / distance);

					middle = ( locations[i+1] + locations[i] ) / 2;

					polygon([ p1, middle, p2 ]);
					polygon([ p3, middle, p4 ]);
			}}
		}

		union() {
			for( i = [0:length-1] ) translate(locations[i]) circle(r = radiuses[i] );
			for( i = [0:length-1] ) {
				if(i < length-1) {
					distance = sqrt(
						(locations[i][0] - locations[i+1][0])^2 +
						(locations[i][1] - locations[i+1][1])^2
					);

					unit    = locations[i+1] - locations[i];
					normal  = [unit[1], unit[0]];

					p1 = locations[i]   + (normal * (radiuses[i] )   / distance);
					p2 = locations[i]   - (normal * (radiuses[i] )   / distance);
					p3 = locations[i+1] + (normal * (radiuses[i+1] ) / distance);
					p4 = locations[i+1] - (normal * (radiuses[i+1] ) / distance);

					m1 = (( locations[i+1] + locations[i] ) / 2) - unit * thick / distance;
					m2 = (( locations[i+1] + locations[i] ) / 2) + unit * thick / distance;

					polygon([ p1, m1, p2 ]);
					polygon([ p3, m2, p4 ]);
			}}
		}
	}}

	render();
}


loc_a  = [0,   0];
loc_b  = [100, 0];
locs   = [loc_a, loc_b];

for(i = [0:len(locs)-1]) translate(locs[i]) pulley(5, 20, 2, 10, $t*360);
//belt_xz(locs, [12, 12], 1.75, 6);
belt_xz_tw(locs, [12, 12], 1.75, 6);
